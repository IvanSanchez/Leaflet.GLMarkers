
// In a console, run:
// rollup -c rollup-config.js

import buble from "@rollup/plugin-buble"


export default {
	input: 'src/GLMarkers.js',
	output: {
		file: 'dist/Leaflet.GLMarkers.js',
	},
	format: 'cjs',
		sourceMap: true,
		plugins: [
// 		require('rollup-plugin-string')({ extensions: ['.glsl'] }),
		buble(),
// 		require('rollup-plugin-commonjs')(),
// 		require('rollup-plugin-node-resolve')()
		]
};

