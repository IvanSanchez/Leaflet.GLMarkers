
# v1.1.0 (2021-07-28)

* Copied logic from Leaflet-Arrugator to make the zoom animation (through
  Leaflet proxy) look decent.
* Switched from eslint to prettier.


